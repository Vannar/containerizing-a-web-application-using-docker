# Running python-server container 

after running mysql container we can run the python container by running following commands;

-> docker build -t server-image .

A image will create with name server-image you can cross verify

-> docker images 

now we can run  the container,

-> docker run --name server-container --network login -p 5000:5000 -d server-image

This will create the server container runnig on port 5000 and in network login

you can cross verify using command

curl -X POST http://localhost:5000/submit

