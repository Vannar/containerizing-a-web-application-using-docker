from flask import Flask, request
import mysql.connector

app = Flask(__name__)

@app.route('/submit', methods=['POST'])
def submit():
    name = request.form.get('name')
    email = request.form.get('email')
    message = request.form.get('message')

    conn = mysql.connector.connect(
        host='mysql-container',
        user='root',
        password='',
        database='test'
    )

    cursor = conn.cursor()
    sql = "INSERT INTO submissions (name, email, message) VALUES (%s, %s, %s)"
    val = (name, email, message)
    cursor.execute(sql, val)
    conn.commit()
    cursor.close()
    conn.close()

    return 'Data inserted successfully'

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
