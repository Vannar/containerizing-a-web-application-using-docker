# Running frontend container

after running backend and database container we left with frontend container

we can run the front-end container using following commnad

docker build -t frontend-image .

A image will create with name frontend-image you can cross verify

-> docker images 

now we can run  the container,

-> docker run --name frontend-container --network login -p 80:80 -d frontend-image

This will create the frontend container runnig on port 80 and in network login

you can cross verify using command

curl -X POST http://localhost
