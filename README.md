This is a simple web application that sends data to the server and stores that data in the database.

# The main focus is to run the application after containerizing using Docker.

Here we use three containers:

mysql-container (for database "mysql" at port 3306)

frontend-container (simple html page hosted using nginx web server at port 80)

server-container (written in Python at port 5000)

All the containers are running on the same Docker network (login).