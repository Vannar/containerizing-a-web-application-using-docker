# First we will create a docker network using following command

-> docker network create login

note* instead of login you can use any name of ypur choice

# now we will setup the mysql docker contaner

run the following Commands

-> docker pull mysql

this will pull the mysql latest image from the docker hub

-> docker images 

you can see there is a image of name mysql with tag latest 

now, we will run the container

-> docker run --name mysql-container --network login -p 3306:3306 -v mydata:/mysql/data  -e MYSQL_ALLOW_EMPTY_PASSWORD=yes  -d mysql


this command will run the container with volumes attached to it.

# for this web application,after running the container, you can create the database with the name "test and the table "submissions," with values (name, email, message).